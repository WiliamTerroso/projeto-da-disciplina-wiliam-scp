#!/bin/bash

if [ "$#" -ne 4 ]; then
    echo "Uso: $0 <arquivo_remoto> <host_remoto> <usuario_remoto> <diretorio_local>"
    exit 1
fi

remote_file=$1
host=$2
user=$3
local_dir=$4

scp "$user@$host:$remote_file" "$local_dir"

