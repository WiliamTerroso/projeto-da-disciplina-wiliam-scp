#!/bin/bash

# Script principal para chamar as funções

# Verifica se os argumentos são fornecidos corretamente
if [ "$#" -ne 5 ]; then
    echo "Uso: $0 <modo_copia> <arquivo_origem> <host_remoto> <usuario_remoto> <destino>"
    exit 1
fi

mode=$1
src=$2
host=$3
user=$4
dest=$5

case $mode in
    "local_to_remote")
        ./copy_to_remote.sh "$src" "$host" "$user" "$dest"
        echo "Cópia de local para remoto concluída."
        ;;
    "remote_to_local")
        ./copy_from_remote.sh "$src" "$host" "$user" "$dest"
        echo "Cópia de remoto para local concluída."
        ;;
    *)
        echo "Modo de cópia inválido. Use 'local_to_remote' ou 'remote_to_local'."
        exit 1
        ;;
esac

