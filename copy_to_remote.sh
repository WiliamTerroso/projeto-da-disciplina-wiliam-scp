#!/bin/bash

if [ "$#" -ne 4 ]; then
    echo "Uso: $0 <arquivo_local> <host_remoto> <usuario_remoto> <diretorio_remoto>"
    exit 1
fi

local_file=$1
host=$2
user=$3
remote_dir=$4

scp "$local_file" "$user@$host:$remote_dir"

